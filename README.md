
# Handling Stocks For Availability

An application in JAVA  with an HTTP API for putting and getting the data in an easy and convenient way.

**Prerequisites:** [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) 

## Getting Started

To install this example application, run the following commands:

```bash
git clone https://github.com/AshithaN/commercetools.git
cd commercetools

Run the project using Java .

Base URIs:http://localhost:8080

Endpoints:
1. POST /updateStock
This endpoint updates the current stock of a particular product.
The JSON body you can put:
{
"id": “string", // unique string to identify the stock, e.g. "000001"
"timestamp": “dateTime" // datetime in UTC, e.g. "2017-07-16T22:54:01.754Z"
"productId": “string", // unique id to identify the products, e.g. "vegetable-123"
"quantity": “integer" // amount of available stock, e.g. 500
}
You get in return:
status-code 201
status-code 204


2. GET /stock?productId=vegetable-123
If you send a request towards this endpoint, you get the current stock available of a particular
product.
The format of the JSON body you should get back:
{
"productId": “string", // id of the requested product, e.g. "vegetable-123"
"requestTimestamp": “dateTime", // datetime in UTC when requested the stock
"stock": {
"id": "string",
"timestamp": "dateTime"
"quantity": "integer"
}
}


3. GET /statistics?time=[today,lastMonth]
When calling this endpoint, we want to receive some statistics about our stocks back. The
available timespans are today (midnight until now) and last month.
The format of the JSON body you should get back:
{
"requestTimestamp": “dateTime", // datetime in UTC when requested the stock
"range": “string", // valid values are "today" and "lastMonth"
"topAvailableProducts": [ // top three products with the highest availability
{
"id": "string",
"timestamp": "dateTime"
"productId": "string",
"quantity": "integer"
},
{
"id": "string",
"timestamp": "dateTime"
"productId": "string",
"quantity": "integer"
},
{
"id": "string",
"timestamp": "dateTime"
"productId": "string",
"quantity": "integer"
}
],
"topSellingProducts": [
{
"productId": "string",
"itemsSold": "integer" // number of products sold within given range
},
{
"productId": "string",
"itemsSold": "integer" // number of products sold within given range
},
{
"productId": "string",
"itemsSold": "integer" // number of products sold within given range
}
]
}

