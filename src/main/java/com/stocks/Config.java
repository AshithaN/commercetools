
package com.stocks;

import javax.sql.DataSource;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

import com.stocks.dao.StockDao;
import com.stocks.dao.StockDaoImpl;
import com.stocks.service.StockService;
import com.stocks.service.StockServiceImpl;

@Configuration
@ComponentScan
public class Config {

	private Logger logger = LoggerFactory.getLogger(this.getClass());

	

	@Bean
	public StockDao stockDao() throws Exception {
		StockDao stockDao = new StockDaoImpl();
		return stockDao;
	}
	
	@Bean
	public StockService stockService() throws Exception {
		StockServiceImpl stockService = new StockServiceImpl();
		stockService.setStockDao(stockDao());
		return stockService;
	}

}
