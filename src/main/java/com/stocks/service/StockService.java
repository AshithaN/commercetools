package com.stocks.service;


import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.stocks.model.Product;
import com.stocks.model.Statistics;


public interface StockService {


	String updateStock(Product product);

	Product getStocks(String productId);
	Statistics getStatistics(LocalDate timeStart, LocalDate timeEnd);

}
