package com.stocks.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.stocks.dao.StockDao;
import com.stocks.model.Product;
import com.stocks.model.Statistics;
import com.stocks.model.Stock;

public class StockServiceImpl implements StockService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	StockDao stockDao;
	
	@Override
	public Product getStocks(String productId) {
		return getStockDao().getStocks(productId);
	}

	@Override
	public Statistics getStatistics(LocalDate timeStart,LocalDate timeEnd) {
		
		return getStockDao().getStatistics(timeStart,timeEnd);
	}

	@Override
	public String updateStock(Product product) {
		return getStockDao().updateStock(product);
	}
	
	public StockDao getStockDao() {
		return stockDao;
	}

	public void setStockDao(StockDao stockDao) {
		this.stockDao = stockDao;
	}


}
