package com.stocks.controller;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjuster;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;

import javax.json.Json;
import javax.json.JsonBuilderFactory;
import javax.json.JsonObject;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.stocks.model.Product;
import com.stocks.model.Statistics;
import com.stocks.model.Stock;
import com.stocks.service.StockService;
import com.stocks.service.StockServiceImpl;

@RestController
@RequestMapping(path ="/")
public class StockController {
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@Autowired
	StockService stockService ;
	
	public enum Time{
		today,
		lastMonth
	}

@GetMapping(path = "/stock", produces="application/json")
@ResponseBody
public JsonProductBuilder getStock(@RequestParam String productId) {
			
			Product response =	stockService.getStocks(productId);
			JsonProductBuilder jsonBuilder  = new JsonProductBuilder();
			return response.getProductId() != null ? jsonBuilder.jsonBuildProduct(response) : jsonBuilder;
		
		
}

@GetMapping(path = "/statistics", produces="application/json")
@ResponseBody
public Statistics getStatistics(@RequestParam Time time) {
   Statistics s = new Statistics();
	
	if(time.toString().equalsIgnoreCase("today")) {
		s=stockService.getStatistics(LocalDate.now(),LocalDate.now());
	}else {
		
		s=stockService.getStatistics(LocalDate.now().minusMonths(1).with(TemporalAdjusters.firstDayOfMonth()),LocalDate.now().minusMonths(1).with(TemporalAdjusters.lastDayOfMonth()));
	}
	s.setRange(time.toString());
  return s;
}

@PostMapping(path = "/updateStock",  produces="application/json", consumes ="application/json")
@ResponseBody
public ResponseEntity updateStock(@RequestBody Map<String,Object> prod) {

	Product p = new Product();
	p.setProductId(prod.get("productId").toString());
	p.setId(prod.get("id").toString());
	p.setQuantity(Integer.valueOf(prod.get("quantity").toString()));
	String flag = stockService.updateStock(p);
	return flag=="N" ? new ResponseEntity(HttpStatus.CREATED): new ResponseEntity(HttpStatus.NO_CONTENT);

}
}	