package com.stocks.controller;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.stocks.model.Product;
import com.stocks.model.Stock;

public class JsonProductBuilder {

	@JsonInclude(Include.NON_NULL)
	private String productId;
	@JsonInclude(Include.NON_NULL)
	private LocalDateTime requestTimeStamp;
	@JsonInclude(Include.NON_NULL)
	private Stock stock;
	
	public String getProductId() {
		return productId;
	}
	public void setProductId(String productId) {
		this.productId = productId;
	}
	public LocalDateTime getRequestTimeStamp() {
		return requestTimeStamp;
	}
	public void setRequestTimeStamp(LocalDateTime requestTimeStamp) {
		this.requestTimeStamp = requestTimeStamp;
	}
	public Stock getStock() {
		return stock;
	}
	public void setStock(Stock stock) {
		this.stock = stock;
	}
	
	public JsonProductBuilder jsonBuildProduct(Product response) {
		JsonProductBuilder jsonBuilder  = new JsonProductBuilder();
		jsonBuilder.setProductId(response.getProductId());
		jsonBuilder.setRequestTimeStamp(response.getStock().getTimestamp());
		Stock jsonStock = new Stock(response.getStock().getId(), response.getTimeStamp());
		jsonStock.setQuantity(response.getQuantity());			
		jsonBuilder.setStock(jsonStock);
		return jsonBuilder;
	}
	
	
}
