package com.stocks.dao;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import com.stocks.model.Product;
import com.stocks.model.Stock;

public class StaticDataInitializer {

	public static List<Product> staticData() {
	   Product product = new Product();
			product.setProductId("Vegetable - 123");
			product.setQuantity(50);
			product.setTimeStamp(LocalDateTime.now());
			product.setItemsSold(10);
			Stock stock = new Stock("1",LocalDateTime.now().minusDays(2));
			product.setStock(stock);
			
			 Product product2 = new Product();
				product2.setProductId("Vegetable - 123");
				product2.setQuantity(50);
				product2.setTimeStamp(LocalDateTime.now());
				product2.setItemsSold(1);
				Stock stock2 = new Stock("2",LocalDateTime.now());
				product2.setStock(stock2);
			
				Product product3 = new Product();
				product3.setProductId("XYZ");
				product3.setQuantity(76);
				product3.setTimeStamp(LocalDateTime.now());
				product3.setItemsSold(3);
				Stock stock3 = new Stock("3",LocalDateTime.now().minusDays(2));
				product3.setStock(stock3);
				
			 Product product4 = new Product();
				product4.setProductId("XYZ");
				product4.setQuantity(65);
				product4.setTimeStamp(LocalDateTime.now());
				product4.setItemsSold(3);
				Stock stock4 = new Stock("4",LocalDateTime.now());
				product4.setStock(stock4);
				
				Product product5 = new Product();
				product5.setProductId("Shoes");
				product5.setQuantity(500);
				product5.setTimeStamp(LocalDateTime.now());
				product5.setItemsSold(8);
				Stock stock5 = new Stock("5",LocalDateTime.now().minusDays(2));
				product5.setStock(stock5);
				
			 Product product6 = new Product();
				product6.setProductId("Shoes");
				product6.setQuantity(500);
				product6.setTimeStamp(LocalDateTime.now());
				product6.setItemsSold(7);
				Stock stock6 = new Stock("6",LocalDateTime.now());
				product6.setStock(stock6);
			
				List<Product> productList = new ArrayList<>();
			productList.add(product);
			productList.add(product2);
			productList.add(product3);
			productList.add(product4);
			productList.add(product5);
			productList.add(product6);
			return productList;
}
}
