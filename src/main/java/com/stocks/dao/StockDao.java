package com.stocks.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import com.stocks.model.Product;
import com.stocks.model.Statistics;
import com.stocks.model.Stock;

public interface StockDao {


	 String updateStock(Product product);

	Product getStocks(String productId);

	Statistics getStatistics(LocalDate timeStart, LocalDate timeEnd);

}

