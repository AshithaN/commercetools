package com.stocks.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.stocks.model.Product;
import com.stocks.model.Statistics;
import com.stocks.model.Stock;

public class StockDaoImpl implements StockDao {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	List<Product> productList = new ArrayList<>();
	
	
	public StockDaoImpl() {
		
	productList.addAll(StaticDataInitializer.staticData());
		 
	}

	Set<String> distinctTopProducts =  new HashSet<>();
	Set<String> distinctSellingProducts =  new HashSet<>();
	
	@Override
	public Product getStocks(String productId) {
		
		List<Product> productStock = new ArrayList<>();
		List<Stock> stockList = new ArrayList<>();
		
		//Get the stock list for the particular product
		stockList = productList.stream()
				.filter(product -> product.getProductId().equals(productId))
				.map(prod -> prod.getStock()).collect(Collectors.toList());
		stockList.sort(Comparator.comparing(Stock::getTimestamp).reversed());;

		// Get the latest stock for the product to return
		if(!stockList.isEmpty()) {
			LocalDateTime maxStockDate = stockList.get(0).getTimestamp();
			productStock = productList.stream()
			.filter(product -> product.getProductId().equals(productId))
			.filter(product -> product.getStock().getTimestamp().equals(maxStockDate))
			.collect(Collectors.toList());
			
		}
		
		
		return !productStock.isEmpty() ? productStock.get(0) : new Product();
		
	}

	@Override
	public Statistics getStatistics(LocalDate timeStart, LocalDate timeEnd) {

		List<Product> productList1 = productList.stream()
				.filter(p -> p.getStock().getTimestamp().toLocalDate().isAfter(timeStart) ||  p.getStock().getTimestamp().toLocalDate().equals(timeStart))
				.filter(p -> p.getStock().getTimestamp().toLocalDate().isBefore(timeEnd) ||  p.getStock().getTimestamp().toLocalDate().equals(timeEnd))
				.collect(Collectors.toList());
		productList1.sort(Comparator.comparing(Product::getQuantity).reversed());
		
		distinctTopProducts.addAll(productList1.stream().map(Product::getProductId).collect(Collectors.toSet()));
		Statistics stat1 = new Statistics();
		stat1.setRequestTimeStamp(productList1.get(0).getStock().getTimestamp());
		
		List<Product> topAvailableProducts = new ArrayList<>();
		for(int i =0; i< 3; i++) {
			if(productList1.size() > i && distinctTopProducts.contains(productList1.get(i).getProductId())) {
				Product p = setTopAvailableProd(productList1,i);
				topAvailableProducts.add(p);
			}
		}
		stat1.setTopAvailableProducts(topAvailableProducts);
		productList1.sort(Comparator.comparing(Product::getItemsSold).reversed());
		Set<String> distinctSellingProducts = productList1.stream().map(Product::getProductId).collect(Collectors.toSet());
	
		List<Product> topSellingProducts = new ArrayList<>();
		for(int i =0; i< 3; i++) {
			if(productList1.size() > i && distinctSellingProducts.contains(productList1.get(i).getProductId())) {
				Product p = setTopSellingMethod(productList1, i);
				topSellingProducts.add(p);
			}
		}
		stat1.setTopSellingProducts(topSellingProducts);
		
		return stat1;
	}

	

	@Override
	public String updateStock(Product product) {
		List<Stock> stockList = new ArrayList<>();
		String outdatedIndicator="Y";
		stockList = productList.stream().filter(prod -> prod.getProductId().equals(product.getProductId()))
				.map(prod -> prod.getStock()).collect(Collectors.toList());
		stockList.sort(Comparator.comparing(Stock::getTimestamp).reversed());;
				
		if(!stockList.isEmpty()) {
			
			if(stockList.get(0).getId().equals(product.getId())) {
				outdatedIndicator ="N";
				LocalDateTime maxStockDate = stockList.get(0).getTimestamp();
				productList.stream().filter(prod -> prod.getProductId().equals(product.getProductId()))
				.filter(prod -> prod.getStock().getTimestamp().equals(maxStockDate))
				.forEach(p ->  p.setQuantity(p.getQuantity() + product.getQuantity()));
				}
			}
		
		return outdatedIndicator;
	}
	
	
	private Product setTopSellingMethod(List<Product> productList1, int i) {
		Product p = new Product();
		p.setProductId(productList1.get(i).getProductId());
		distinctSellingProducts.remove(productList1.get(i).getProductId());
		p.setItemsSold(productList1.get(i).getItemsSold());
		return p;
	}

	private Product setTopAvailableProd( List<Product> productList1 ,int i) {
		Product p = new Product();
		p.setProductId(productList1.get(i).getProductId());
		distinctTopProducts.remove(productList1.get(i).getProductId());
		p.setId(productList1.get(i).getStock().getId());
		p.setTimeStamp(productList1.get(i).getTimeStamp());
		p.setQuantity(productList1.get(i).getQuantity());
		return p;
	}


}
