package com.stocks.model;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.annotation.JsonProperty;

public class Product {

	@JsonProperty
	@JsonInclude(Include.NON_NULL)
	private String productId;
	@JsonProperty
	@JsonInclude(Include.NON_NULL)
	private LocalDateTime timeStamp;
	
	@JsonProperty
	@JsonInclude(Include.NON_DEFAULT)
	private int quantity;
	
	@JsonProperty
	@JsonInclude(Include.NON_DEFAULT)
	private int itemsSold;
	
	@JsonIgnore
	private LocalDateTime itemsSoldDate;
	
	@JsonProperty
	@JsonInclude(Include.NON_NULL)
	private Stock stock;
	
	@JsonProperty
	@JsonInclude(Include.NON_NULL)
	private String id;

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public LocalDateTime getTimeStamp() {
		return timeStamp;
	}

	public void setTimeStamp(LocalDateTime timeStamp) {
		this.timeStamp = timeStamp;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public int getItemsSold() {
		return itemsSold;
	}

	public void setItemsSold(int itemsSold) {
		this.itemsSold = itemsSold;
	}

	public LocalDateTime getItemsSoldDate() {
		return itemsSoldDate;
	}

	public void setItemsSoldDate(LocalDateTime itemsSoldDate) {
		this.itemsSoldDate = itemsSoldDate;
	}

	public Stock getStock() {
		return stock;
	}

	public void setStock(Stock stock) {
		this.stock = stock;
	}

	public String getId() {
		return id;
	}

	public void setId(String string) {
		this.id = string;
	}

	public Product(String productId, LocalDateTime timeStamp, int quantity, int itemsSold, LocalDateTime itemsSoldDate,
			Stock stock, String id) {
		super();
		this.productId = productId;
		this.timeStamp = timeStamp;
		this.quantity = quantity;
		this.itemsSold = itemsSold;
		this.itemsSoldDate = itemsSoldDate;
		this.stock = stock;
		this.id = id;
	}

	public Product() {
		super();
		// TODO Auto-generated constructor stub
	}

		
	
	
}
