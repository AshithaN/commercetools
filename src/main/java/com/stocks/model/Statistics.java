package com.stocks.model;


import java.time.LocalDateTime;
import java.util.List;

import javax.json.JsonObject;

import com.fasterxml.jackson.annotation.JsonInclude;

public class Statistics {
	
	@JsonInclude
	private LocalDateTime requestTimeStamp;
	
	@JsonInclude
	private String range;
	
	@JsonInclude
	private List<Product> topAvailableProducts;
	
	@JsonInclude
	private List<Product> topSellingProducts;

	public LocalDateTime getRequestTimeStamp() {
		return requestTimeStamp;
	}

	public void setRequestTimeStamp(LocalDateTime requestTimeStamp) {
		this.requestTimeStamp = requestTimeStamp;
	}

	public String getRange() {
		return range;
	}

	public void setRange(String range) {
		this.range = range;
	}

	public List<Product> getTopAvailableProducts() {
		return topAvailableProducts;
	}

	public void setTopAvailableProducts(List<Product> topAvailableProducts) {
		this.topAvailableProducts = topAvailableProducts;
	}

	public List<Product> getTopSellingProducts() {
		return topSellingProducts;
	}

	public void setTopSellingProducts(List<Product> topSellingProducts) {
		this.topSellingProducts = topSellingProducts;
	}

	public Statistics(LocalDateTime requestTimeStamp, String range, List<Product> topAvailableProducts,
			List<Product> topSellingProducts) {
		super();
		this.requestTimeStamp = requestTimeStamp;
		this.range = range;
		this.topAvailableProducts = topAvailableProducts;
		this.topSellingProducts = topSellingProducts;
	}

	public Statistics() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	

}
