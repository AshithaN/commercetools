package com.stocks.model;

import java.time.LocalDateTime;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

public class Stock {
	
	@JsonProperty
	private String id;
	
	@JsonProperty
	@JsonInclude(Include.NON_NULL)
	private LocalDateTime timestamp;
	
	@JsonInclude(Include.NON_DEFAULT)
	private int quantity;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public LocalDateTime getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(LocalDateTime timestamp) {
		this.timestamp = timestamp;
	}
	
	

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public Stock() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Stock(String id, LocalDateTime timestamp) {
		super();
		this.id = id;
		this.timestamp = timestamp;
		
	}
	
	

}
